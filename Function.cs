using Amazon.DynamoDBv2;
using Amazon.Lambda.Core;
using Amazon.Lambda.DynamoDBEvents;
using Amazon.Lambda.RuntimeSupport;
using Amazon.Lambda.Serialization.SystemTextJson;
using Amazon.XRay.Recorder.Handlers.AwsSdk;
using CSharpFunctionalExtensions;
using Kravets.BrainBooster.Aws.DynamoDb;
using Kravets.BrainBooster.Aws.DynamoDb.Parsers;
using Kravets.BrainBooster.Domain.Exceptions;
using Kravets.BrainBooster.Domain.Shared;
using Kravets.BrainBooster.Flows.Storage;
using Kravets.BrainBooster.Flows.Storage.Converters;
using Kravets.BrainBooster.Flows.Storage.Parsers;
using Kravets.BrainBooster.Logging;
using Kravets.BrainBooster.Telegram.Domain;
using Kravets.BrainBooster.Telegram.Storage;
using Serilog;
using static Kravets.BrainBooster.Aws.Lambda.Decorators;
using static Kravets.BrainBooster.Aws.Lambda.DynamoDb.Decorators;

AWSSDKHandler.RegisterXRayForAllServices();

var logger = new LoggerConfiguration()
    .ConfigureDefault()
    .CreateLogger();

var unitOfKnowledgeIdParser = new UnitOfKnowledgeIdParser();
var userIdParser = new UserIdParser();
var dateTimeParser = new DateTimeParser();
var unitOfKnowledgeParser = new UnitOfKnowledgeParser(unitOfKnowledgeIdParser, userIdParser, dateTimeParser);
var unitOfKnowledgeConverter = new UnitOfKnowledgeConverter();

var dynamoDb = new AmazonDynamoDBClient();
var flowsRepository = new FlowsRepository(dynamoDb, unitOfKnowledgeParser, unitOfKnowledgeConverter);
var telegramContextsRepository = new TelegramContextsRepository(dynamoDb);

var handler = async (DynamoDBEvent.DynamodbStreamRecord record, ILambdaContext context) =>
{
    logger.Verbose("Processing event");

    var unitOfKnowledge = unitOfKnowledgeParser.Parse(record.Dynamodb.NewImage);

    var (contextFound, telegramUserContext) = await telegramContextsRepository.Get(unitOfKnowledge.UserId);

    if (!contextFound)
    {
        logger
            .ForContext("UserId", unitOfKnowledge.UserId)
            .Error("Telegram user context is not found");
        throw new NotFoundException("Telegram user context is not found");
    }

    if (telegramUserContext.State is not PendingForNextUnitState)
    {
        logger
            .ForContext("UserId", unitOfKnowledge.UserId)
            .Information("Telegram user context state is not PendingForNextUnitState");
        return;
    }

    var units = await flowsRepository.Get(unitOfKnowledge.UserId, Limit.One);

    var (isError, error) = units.Any()
        ? telegramUserContext.State.SetNextUnit(units.First(), telegramUserContext)
        : telegramUserContext.State.SetInitialState(telegramUserContext);

    if (isError)
    {
        logger.Error(error.Message);
        throw new ProhibitedStateTransitionException(error);
    }

    await telegramContextsRepository.Save(telegramUserContext);
};

var decoratedHandler = Decorate(
    handler,
    WithTraceId
);

await LambdaBootstrapBuilder.Create(decoratedHandler, new DefaultLambdaJsonSerializer())
    .Build()
    .RunAsync();
