using CSharpFunctionalExtensions;
using Kravets.BrainBooster.Domain.Abstractions;
using Kravets.BrainBooster.Domain.Errors;

namespace Kravets.BrainBooster.Domain.Scheduling;

public class Schedule
{
    private Schedule(
        ScheduleId id,
        DateTime atUtc,
        ScheduleState state,
        ScheduleData data,
        DateTime createdOnUtc,
        Maybe<DateTime> completedOnUtc,
        Maybe<DateTime> cancelledOnUtc)
    {
        Id = id;
        AtUtc = atUtc;
        State = state;
        Data = data;
        CreatedOnUtc = createdOnUtc;
        CompletedOnUtc = completedOnUtc;
        CancelledOnUtc = cancelledOnUtc;
    }
    
    public ScheduleId Id { get; }
    public DateTime AtUtc { get; }
    public ScheduleState State { get; private set; }
    public ScheduleData Data { get; }
    public DateTime CreatedOnUtc { get; }
    public Maybe<DateTime> CompletedOnUtc { get; private set; }
    public Maybe<DateTime> CancelledOnUtc { get; private set; }

    public Maybe<ValidationError> Complete(IDateTimeProvider dateTimeProvider)
    {
        if (State != ScheduleState.Pending)
        {
            return new ValidationError("Can't complete schedule. Current state is not pending");
        }
        
        State = ScheduleState.Completed;
        CompletedOnUtc = dateTimeProvider.UtcNow;
        return Maybe.None;
    }
    
    public Maybe<ValidationError> Cancel(IDateTimeProvider dateTimeProvider)
    {
        if (State != ScheduleState.Pending)
        {
            return new ValidationError("Can't cancel schedule. Current state is not pending");
        }
        
        State = ScheduleState.Cancelled;
        CancelledOnUtc = dateTimeProvider.UtcNow;
        return Maybe.None;
    }
    
    public static Result<Schedule, ValidationError> Create(
        ScheduleId id,
        DateTime atUtc,
        ScheduleState state,
        ScheduleData data,
        DateTime createdOnUtc,
        Maybe<DateTime> completedOnUtc,
        Maybe<DateTime> cancelledOnUtc,
        IDateTimeProvider dateTimeProvider)
    {
        var now = dateTimeProvider.UtcNow;
        
        if (atUtc <= now)
        {
            return new ValidationError($"At should be greater than {now}");
        }

        if (completedOnUtc.HasValue && completedOnUtc.Value < createdOnUtc)
        {
            return new ValidationError(
                $"Completed date should be greater of equal than created date ({createdOnUtc})");
        }

        if (completedOnUtc.HasValue && completedOnUtc.Value < atUtc)
        {
            return new ValidationError($"Completed date should be greater of equal than at date ({atUtc})");
        }
        
        if (cancelledOnUtc.HasValue && cancelledOnUtc.Value < createdOnUtc)
        {
            return new ValidationError(
                $"Cancelled date should be greater of equal than created date ({createdOnUtc})");
        }

        if (cancelledOnUtc.HasValue && cancelledOnUtc.Value < atUtc)
        {
            return new ValidationError($"Cancelled date should be greater of equal than at date ({atUtc})");
        }

        if (state == ScheduleState.Completed && completedOnUtc.HasNoValue)
        {
            return new ValidationError($"Completion date should be set");
        }
        
        if (state == ScheduleState.Cancelled && cancelledOnUtc.HasNoValue)
        {
            return new ValidationError($"Cancellation date should be set");
        }

        return new Schedule(id, atUtc, state, data, createdOnUtc, completedOnUtc, cancelledOnUtc);
    }
    
    public static Result<Schedule, ValidationError> Create(
        DateTime atUtc,
        ScheduleData data,
        IGuidProvider guidProvider,
        IDateTimeProvider dateTimeProvider)
    {
        return Create(
            ScheduleId.New(guidProvider),
            atUtc,
            ScheduleState.Pending,
            data,
            dateTimeProvider.UtcNow,
            Maybe.None,
            Maybe.None,
            dateTimeProvider);
    }
}