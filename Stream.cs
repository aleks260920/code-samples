using CSharpFunctionalExtensions;
using Kravets.BrainBooster.Domain.Abstractions;
using Kravets.BrainBooster.Domain.Errors;
using Kravets.BrainBooster.Domain.Stream.SchedulingStrategies;

namespace Kravets.BrainBooster.Domain.Stream;

public class Stream
{
    private Stream(StreamId id, string name, Settings settings)
    {
        Id = id;
        Name = name;
        Settings = settings;
    }

    public StreamId Id { get; }
    public string Name { get; }
    public Settings Settings { get; }

    public static Result<Stream, ValidationError> Create(StreamId id, string name, Settings settings)
    {
        const int maxNameLength = 10000;
        const int minNameLength = 1;

        if (name.Length is < minNameLength or > maxNameLength)
        {
            return new ValidationError($"Name length should be between {minNameLength} and {maxNameLength}");
        }

        return new Stream(id, name, settings);
    }

    public static Stream CreateDefault(IGuidProvider guidProvider) =>
        new (StreamId.New(guidProvider), "default", new Settings(ExponentialStrategy.Default));

    public static Stream[] CreateDefaultSet(IGuidProvider guidProvider) =>
        new []
        {
            CreateDefault(guidProvider),
            new Stream(
                StreamId.New(guidProvider),
                "every day",
                new Settings(LinearStrategy.EveryDayStrategy)),
            new Stream(
                StreamId.New(guidProvider),
                "first three days",
                new Settings(
                    CustomStrategy.Create(
                        new []{ TimeSpan.FromDays(1), TimeSpan.FromDays(1), TimeSpan.FromDays(1) }).Value))
        };
}
